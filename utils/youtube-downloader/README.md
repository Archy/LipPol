# YouTube downloader

To download the YouTube videos for our dataset, modify the constants at the top of `download.py` and the calls to `download` at the bottom, then run it:

```
./download.py
```

A directory named `dataset` will be created for the downloaded videos and their subtitles. Temporary data will be placed in `cache` so that you can interrupt downloading at any time and resume it by just running the program again.

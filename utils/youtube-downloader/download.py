#!/usr/bin/env python3

import collections
import contextlib
import os
import re
import subprocess
import sys
import shutil
import json
from termcolor import cprint

# Settings.
HEIGHT_LIMIT = 1080 # Use to None to disable.
DOWNLOAD_AUDIO = False
SLEEP_INTERVAL = 0 # Set to a number (seconds) to introduce a delay before each download. Set to [X, Y] for a random delay of X to Y seconds before each download.

def warning(message):
    print("WARN: {}".format(message), file=sys.stderr)

def error(message):
    print("ERROR: {}".format(message), file=sys.stderr)

def fatal_error(message, exit_status=1):
    error(message)
    sys.exit(exit_status)

def header(string, level=1):
    if level == 1:
        attrs = ["underline"]
    else:
        attrs = ["bold"]
    cprint(string, attrs=attrs)

def require_commands(*program_names):
    for program in program_names:
        if shutil.which(program) is None:
            fatal_error("Required command `{}` not found.".format(program))

def this_script_dir():
    return os.path.dirname(os.path.realpath(__file__))

require_commands("youtube-dl")

@contextlib.contextmanager
def cd(new_dir):
    prev_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(prev_dir)

def ytdl_format():
    # Youtube-dl format parameter. (The default is `bestvideo+bestaudio/best`.)

    if HEIGHT_LIMIT is not None:
        height_param = "[height<={}]".format(HEIGHT_LIMIT)
    else:
        height_param = ""

    if DOWNLOAD_AUDIO:
        return "bestvideo{0}+bestaudio/best{0}".format(height_param)
    return "bestvideo{}".format(height_param)

MAIN_DIR = this_script_dir()
assert os.path.isabs(MAIN_DIR)

def get_path(relpath, is_dir=True, mkdir=True):
    if isinstance(relpath, collections.Iterable) \
       and not isinstance(relpath, (str, bytes)):
        relpath = os.path.join(*relpath)
    path = os.path.normpath(os.path.join(MAIN_DIR, relpath))

    if mkdir:
        if is_dir:
            os.makedirs(path, exist_ok=True)
        else:
            os.makedirs(os.path.dirname(path), exist_ok=True)

    return path

ytdl_archive_file = get_path("cache/youtube-dl-download-archive.txt", is_dir=False)
ytdl_cache_dir = get_path("cache/youtube-dl")

def ytdl_base_args(sleep=True):
    args = [
        "youtube-dl",
        "--retries", "10", # The retry interval is currently not customizable.
        "--cache-dir", ytdl_cache_dir, # The default is `~/.cache/youtube-dl`.
        "--geo-bypass"] # Bypass geographic restriction by faking the X-Forwarded-For HTTP header

    if sleep:
        if isinstance(SLEEP_INTERVAL, collections.Iterable):
            args.extend([
                "--min-sleep-interval", str(SLEEP_INTERVAL[0]),
                "--max-sleep-interval", str(SLEEP_INTERVAL[1])])
        elif SLEEP_INTERVAL > 0:
            args.extend(["--sleep-interval", str(SLEEP_INTERVAL)])

    return args

def download_raw(urls=(), extra_args=(), directory=None, mkdir=True):
    args = ytdl_base_args() + [
        "-f", ytdl_format(),
        "-o", "%(title)s -- %(id)s, %(license)s.%(ext)s",
        "--write-sub", "--sub-lang", "pl",
        "--convert-subs", "srt", # Convert to the most widely supported subtitle format.
        "--download-archive", ytdl_archive_file, # Store successfully downloaded URLs in this file. Skip them on subsequent runs.
        "--write-info-json"] # Write video metadata to an .info.json file.

    args.extend(extra_args)
    args.append("--")
    args.extend(urls)

    if directory is not None and mkdir:
        os.makedirs(directory, exist_ok=True)

    kwargs = {}
    if directory is not None:
        kwargs["cwd"] = directory

    subprocess.run(args, check=True, **kwargs)

    # Some options that may be useful:
    #   --embed-subs  Embed subtitles in the video (only for mp4, webm and mkv).
    #   --add-metadata  Write metadata to the video file.
    #   --socket-timeout  Time to wait before giving up, in seconds.
    #   --match-filter  Download only videos matching a filter. See fields below.
    #   -o  Output template. The default is `%(title)s-%(id)s.%(ext)s.`. See fields below.
    #   --exec  Execute a command on the file after downloading. Example: `--exec 'adb push {} /sdcard/Music/ && rm {}'`.
    #   --restrict-filenames  Restrict filenames to ASCII without spaces and "&".

    # Fields that may be useful:
    #   - license  String.
    #   - duration  Numeric.
    #   - age_limit  Age restriction. Numeric (years).
    #   - upload_date  String (YYYYMMDD).
    #   - width  Numeric.
    #   - height  Numeric.
    #   - vbr  Average video bitrate. Numeric (KB/s).
    #   - playlist  Name or id of the playlist that contains the video. String.
    #   - playlist_id  Playlist identifier. String.
    #   - playlist_title  Playlist title. String.
    #   - playlist_index  Index of the video in the playlist padded with leading zeros according to the total length of the playlist. Numeric.
    #   - uploader  Full name of the video uploader. String.
    #   - uploader_id  Nickname or id of the video uploader. String.

    # To list temporary files, use `find . -name '*.part' -or -name '*.ytdl'`. To remove, add `-delete`.

def videos_with_subs(urls=()):
    args = ytdl_base_args() + [
        "--dump-json", # Just print JSON, don't download or print progress information.
        "--all-subs", # Needed for youtube-dl to check for subtitles (otherwise the `subtitles` key will always be `{}`). You can also use `--write-sub --sub-lang LANG`.
        "--no-warnings"] # Suppress warnings (so that we're not spammed with `WARNING: video doesn't have subtitles` for each video without subtitles).

    args.append("--")
    args.extend(urls)

    process = subprocess.Popen(args, stdout=subprocess.PIPE, bufsize=1)  # check=True

    result = []
    for i_video, line in enumerate(process.stdout): # We iterate line by line to get real-time progress and save memory (each line is ~45 kB).
        data = json.loads(line)

        if "pl" in data["subtitles"]:
            result.append(data["webpage_url"])

        print("\rChecked {} total, {} with subs, last: <{}> \033[K".format(
            i_video + 1, len(result), data["webpage_url"]),
              end='')
    print("")

    if process.wait() != 0:
        fatal_error("Youtube-dl failed.")

    return result

def download(urls=(), extra_args=(), directory=None, mkdir=True):
    header(directory)

    # If the user has provided direct video URLs only, assume that they've already checked for subtitles.
    check_subs = not all(
        re.match(r"https?://(?:www\.)?(?:youtu\.be|youtube\.com)/watch\?v=", url) is not None
        for url in urls)

    if check_subs:
        header("Checking which videos have subtitles", level=2)
        urls = videos_with_subs(urls)

    header("Downloading videos", level=2)
    download_raw(urls, extra_args, directory, mkdir)

    print("")

os.chdir(get_path("dataset"))

download(
    directory="Historia bez cenzury",
    urls=[
        "https://www.youtube.com/watch?v=uhSSVsXBPk4",
        "https://www.youtube.com/watch?v=wyB1hmmRJ0U",
        "https://www.youtube.com/watch?v=hzbnCtvaueo"])

download(
    directory="Polimaty",
    urls=[
        "https://www.youtube.com/watch?v=Vc7OtYQZ0zc",
        "https://www.youtube.com/watch?v=XiC7a27RM6o"])

download(
    directory="Cukier",
    urls=[
        "https://www.youtube.com/watch?v=-x8tLxPh0Bw",
        "https://www.youtube.com/watch?v=DtLjSY63tBw"])

download(
    directory="TV Dami Jelenia Góra",
    urls=["https://www.youtube.com/watch?v=jHe-081QDko"])

download(
    directory="Strefa Psyche Uniwersytetu SWPS",
    urls=[
        "https://www.youtube.com/watch?v=MeRYNeaavXs",
        "https://www.youtube.com/watch?v=-lLHs5DHRfI",
        "https://www.youtube.com/watch?v=kTEL3Xum8qM"])

download(
    directory="Misc. Q&A",
    urls=[
        "https://www.youtube.com/watch?v=j_zkGBHwngU",
        "https://www.youtube.com/watch?v=vKr-cR61g7o",
        "https://www.youtube.com/watch?v=DW1PNHLuong"])

download(
    directory="Other",
    urls=[
        "https://www.youtube.com/watch?v=h5bu5f2ZGXs",
        "https://www.youtube.com/watch?v=UzV_D7AJgr0",
        "https://www.youtube.com/watch?v=5RTcsc5FYwc",
        "https://www.youtube.com/watch?v=njzV1xa-zno",
        "https://www.youtube.com/watch?v=8y5NU6E9u8U",
        "https://www.youtube.com/watch?v=wFCf2t0p3fE",
        "https://www.youtube.com/watch?v=cI2MIFZTr3k"])

download(
    directory="Rajkow.pl",
    urls=["https://www.youtube.com/channel/UCCA28whEAyb3x49noGJqvxQ"])

download(
    directory="LekarzMedycynyPracy",
    urls=["https://www.youtube.com/user/LekarzMedycynyPracy"])

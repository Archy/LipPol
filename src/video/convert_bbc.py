import os
import cv2
import time

from src.video.mouth_extractor import MouthExtractor
from src.dataset.bbc_set import load_bbc, SetType
from src.config.config import config


base_src_path = os.path.join(config['bbc_base_path'], 'mvlrs_v1', 'main')
src_ext = '.mp4'
base_dst_path = r'D:\Datasets\BBC_reduced\small\small\mvlrs_v1\mouth'
dst_ext = '.avi'
fps = 5

sets_to_parse = [SetType.TRAIN, SetType.VAL]


def convert_bbc():
    """Remember to set config::max_allowed_frames to some high value if you don't want to skip anything"""
    bbc = load_bbc()
    extractor = MouthExtractor()
    saved = 0
    total = 0

    for s_type in sets_to_parse:
        for sample in bbc.sets[s_type]:
            start_time = time.time()
            total += 1

            src_path = get_src_path(sample)
            dst_path = get_dst_path(sample)

            if os.path.exists(dst_path):
                print('%s already parsed (exists in %s). Skipping' % (src_path, dst_path))
                continue

            try:
                frames = extractor.extract_mouth_bbc(src_path)
                save_frames(dst_path, frames)
                saved += 1
                print('Parsing %d frames took %fs' % (len(frames), time.time() - start_time))
            except Exception as e:
                print('Failed to parse %s, skipping. %s' % (src_path, e))

    print('Succesfully parse %d out of %d' % (saved, total))


def save_frames(dst_path, frames):
    video = cv2.VideoWriter(dst_path, cv2.VideoWriter_fourcc(*"MJPG"), fps, (100, 50))
    try:
        for f in frames:
            video.write(f)
    finally:
        video.release()


def get_dst_path(sample):
    subdir, name = sample.split('/')

    dst_path = os.path.join(base_dst_path, subdir)
    os.makedirs(dst_path, exist_ok=True)
    return os.path.join(dst_path, name) + dst_ext


def get_src_path(sample):
    subdir, name = sample.split('/')
    return os.path.join(base_src_path, subdir, name) + src_ext


if __name__ == "__main__":
    convert_bbc()

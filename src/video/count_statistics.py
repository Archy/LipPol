import os
import cv2
from src.config.config import config
from src.encoding.encoders.transcription_exception import TranscriptionException
from src.encoding.encoders.visems_encoder import VisemsEncoder

encoder = VisemsEncoder()
failed_files = 0


def count_max_bbc_length():
    """counts longest transcriptions in bbc video (in any subsets - pretrain/train/val/test)"""
    mvlrs_v1_dir = os.path.join(config['bbc_base_path'], 'mvlrs_v1')

    pretrain_dir = os.path.join(mvlrs_v1_dir, 'pretrain')
    pretrain_max, pretrain_frames = _parse_dir(pretrain_dir, _parse_file)

    main_dir = os.path.join(mvlrs_v1_dir, 'main')
    main_max, main_frames = _parse_dir(main_dir, _parse_file)

    print('Pretrain max visemes: ', pretrain_max)
    print('Pretrain max frames: ', pretrain_frames)

    print('Main max visemes: ', main_max)
    print('Main max frames: ', main_frames)

    print('Total max visemes: ', max([main_max, pretrain_max]))
    print('Total max frames: ', max([main_frames, pretrain_frames]))

    print('Failed files: ', failed_files)


def _parse_dir(base_path, parse_file):
    txt_count = 0
    visemes_max = -1

    mp4_count = 0
    frames_max = -1

    for dirpath, dirnames, filenames in os.walk(base_path):
        for filename in filenames:
            extension = os.path.splitext(filename)[1]
            if extension == '.txt':
                txt_count += 1
                with open(os.path.join(dirpath, filename), encoding='UTF-8') as file:
                    tmp = parse_file(file)
                    visemes_max = max(visemes_max, tmp)

            elif extension == '.mp4':
                mp4_count += 1
                tmp = _parse_clip(os.path.join(dirpath, filename))
                frames_max = max(frames_max, tmp)
        print('\tparsed: ', txt_count)

    assert txt_count == mp4_count
    print('Parsed %d examples' % txt_count)
    return visemes_max, frames_max


def _parse_file(file):
    global failed_files

    line = file.readline()
    text = line.replace('Text:', '').lstrip().rstrip()
    try:
        encoded = encoder.sentence_to_visems(text, 'english')
        as_visemes = " ".join(list(map(lambda x: x[1], encoded)))
        return len(as_visemes)
    except TranscriptionException:
        failed_files += 1
        return -1


def _parse_clip(filepath):
    vidcap = cv2.VideoCapture(filepath)
    count = 0

    success, image = vidcap.read()
    while success:
        success, image = vidcap.read()
        count = count + 1
    return count


if __name__ == "__main__":
    count_max_bbc_length()


# OUT:
# Parsed 9241 examples
# Pretrain max visemes:  631
# Pretrain max frames:  1279
# Main max visemes:  89
# Main max frames:  154
# Total max visemes:  631
# Total max frames:  1279
# Failed files:  1964

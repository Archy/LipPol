class MouthNotFoundException(Exception):
    """Indicates failure to detect mouth in a video frame"""
    pass

import numpy as np
import logging
import cv2
import dlib
import time

from src.video.mouth_not_found_exception import MouthNotFoundException
from src.config.config import config


class MouthExtractor:

    def __init__(self, mouth_width=100, mouth_height=50, horizontal_pad=0.19) -> None:
        """ Output images dimensions have to be 100x50 to work with rizkiarm lipnet """
        super().__init__()
        self.mouth_width = mouth_width
        self.mouth_height = mouth_height
        self.horizontal_pad = horizontal_pad
        self.max_wrong_frames = config['max_wrong_frames']
        self.max_allowed_frames = config['max_allowed_frames']
        # detectors:
        self.face_detector = dlib.get_frontal_face_detector()
        self.mouth_detector = dlib.shape_predictor(config['face_predictor_path'])

    def extract_mouth_bbc(self, video_path):
        start = time.time()

        vidcap = cv2.VideoCapture(video_path)
        try:
            mouth_frames = []
            count = 0
            wrong_frames = 0

            success, image = vidcap.read()
            while success:
                mouth_extracted = self._extract_mouth(image)
                if mouth_extracted is None:
                    wrong_frames += 1
                else:
                    mouth_frames.append(mouth_extracted)
                success, image = vidcap.read()
                count = count + 1
            if wrong_frames / count > self.max_wrong_frames:
                raise MouthNotFoundException('Too many frames missing mouth: %d from %d (%s)' % (wrong_frames, count, video_path))
            if count > self.max_allowed_frames:
                raise MouthNotFoundException('Too long movie clip')

            logging.info('Video processing (%d frames) took: %f', count, time.time() - start)
            return mouth_frames
        finally:
            vidcap.release()

    def _extract_mouth(self, frame):
        start = time.time()

        face_regions = self.face_detector(frame, 1)
        shape = None
        for k, d in enumerate(face_regions):
            shape = self.mouth_detector(frame, d)
            if shape is not None:
                break
        if shape is None:  # no face on image
            return None

        np_mouth_points = np.array(list(map(lambda p: (p.x, p.y), shape.parts()[48:68:1])))

        # 'borrowed' from https://github.com/rizkiarm/LipNet/blob/master/lipnet/lipreading/videos.py
        mouth_centroid = np.mean(np_mouth_points[:, -2:], axis=0)

        mouth_left = np.min(np_mouth_points[:, :-1]) * (1.0 - self.horizontal_pad)
        mouth_right = np.max(np_mouth_points[:, :-1]) * (1.0 + self.horizontal_pad)

        normalize_ratio = self.mouth_width / float(mouth_right - mouth_left)

        new_img_shape = (int(frame.shape[0] * normalize_ratio), int(frame.shape[1] * normalize_ratio))
        resized_img = cv2.resize(frame, new_img_shape)

        mouth_centroid_norm = mouth_centroid * normalize_ratio

        mouth_l = int(mouth_centroid_norm[0] - self.mouth_width / 2)
        mouth_r = int(mouth_centroid_norm[0] + self.mouth_width / 2)
        mouth_t = int(mouth_centroid_norm[1] - self.mouth_height / 2)
        mouth_b = int(mouth_centroid_norm[1] + self.mouth_height / 2)

        mouth_crop_image = resized_img[mouth_t:mouth_b, mouth_l:mouth_r]

        logging.debug('Frame processing took: %f', time.time() - start)
        return mouth_crop_image

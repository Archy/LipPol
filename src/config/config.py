import os

config = dict(
    use_rizkiarm_weights=True,

    output_size=17,  # 15 visemes + 1 space + 1 empty for ctc (includes only english visemes for now)

    frame_width=100,  # mouth frame dimensions can't be changed if you want to used pretrained rizkiarm weights
    frame_height=50,
    max_allowed_frames=120,  # longer movies are dropped due to memory issues. Needs to be adjusted together with batch size

    epochs=500,
    validation_steps=10,  # number of validation batches to run validation on every epoch
    steps_per_epoch=400,  # has to be set, since there can be invalid samples (we don't know the number of available samples beforehand)
    batch_size=10,
    learing_rate=0.0001,

    # mouth extraction config below; not used in lippol itself
    max_wrong_frames=0.2,  # max no of frames missing mouth (in %)

    # Print the names of files as they're loaded. Useful for determining which
    # files cause decoding errors (e.g. `[mjpeg @ 0x7f69b8004180] error dc`).
    print_mouths_when_loading=True,
)

if 'DATASET' in os.environ:  # Running in Docker.
    assert 'WORKDIR' in os.environ
    assert 'TRAINING_DIR' in os.environ
    config['bbc_base_path'] = os.environ['DATASET']
    config['rizkiarm_weights_path'] = os.path.join(os.environ['WORKDIR'], 'utils/weights/rizkiarm/unseen-weights178.h5')
    config['checkpoints_path'] = os.path.join(os.environ['TRAINING_DIR'], 'weights', '{epoch:02d}.h5')
    config['csvlog_path'] = os.path.join(os.environ['TRAINING_DIR'], 'training.csv.log')
    config['train_log_path'] = os.path.join(os.environ['TRAINING_DIR'], 'training.log')
    config['face_predictor_path'] = os.path.join(os.environ['WORKDIR'], 'utils/face_extractor/dlib-models/shape_predictor_68_face_landmarks.dat')
else:
    config['bbc_base_path'] = r'D:\Datasets\small\small'
    config['rizkiarm_weights_path'] = r'C:\Users\Jan\Desktop\LipNet\LipPol\utils\weights\rizkiarm\unseen-weights178.h5'
    config['checkpoints_path'] = r'D:\Datasets\lippol_checkpoints\weights{epoch:02d}.h5'
    config['csvlog_path'] = r'C:\Users\Jan\Desktop\LipNet\LipPol\training.csv.log'
    config['train_log_path'] = r'C:\Users\Jan\Desktop\LipNet\LipPol\training.log'
    config['face_predictor_path'] = r'C:\Users\Jan\Desktop\LipNet\LipPol\utils\face_extractor\dlib-models\shape_predictor_68_face_landmarks.dat'

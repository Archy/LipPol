from src.encoding.encoders.eng_encoder import EnglishEncoder
from src.encoding.encoders.pl_encoder import PolishEncoder
import string


class VisemsEncoder:
    """Text to visems encoder"""

    def __init__(self):
        self.encoders = {
            'english': EnglishEncoder(),
            'polish': PolishEncoder()
        }
        self.punctuation_table = str.maketrans({key: None if key != '\'' else '\'' for key in string.punctuation})

    def word_to_visems(self, word, language):
        """Converts a word to visemes representation"""
        return self.encoders[language].to_visems(word.lower())

    def sentence_to_visems(self, sentence, language):
        """Converts a sentence to list of (word, visemes). Preservers words order"""
        encoded = []
        for word in sentence.translate(self.punctuation_table).split():
            encoded.append((word, self.word_to_visems(word, language)))
        return encoded

class UniversalEncoder:
    def __init__(self, phonemesEncoder, phoneme_viseme_map, ignored):
        self.phonemesEncoder = phonemesEncoder
        self.phoneme_viseme_map = phoneme_viseme_map
        self.ignored = ignored
        self._create_phonemes_group_list()

    def to_visems(self, word):
        """Converts a word to visems"""
        if word in ('', ' '):
            return word

        visemes_list = []   # use ~efficient way of building visemes string
        phonetic = self.phonemesEncoder.to_phonetics(word)
        i = 0
        while i < len(phonetic):
            s = phonetic[i]

            # check if current phoneme is a special sign that we ignore
            if s in self.ignored:
                i += 1
                continue

            # Check if the current phoneme can be joined with following ones, and then converted to a viseme
            # eg. eɪ -> e, ɛə -> E, t͡ʃ -> S
            for group_length in self.groups_lengths:
                group = phonetic[i:i+group_length] if (i+group_length-1) < len(phonetic) else None
                if group in self.phoneme_viseme_map:
                    s = group
                    i += group_length - 1
                    break

            if s not in self.phoneme_viseme_map:
                print('\t%s not found in visemes map' % s)
                print('\t%s   -   %s' % (word, phonetic))
                visemes_list.append('?')
            else:
                visemes_list.append(self.phoneme_viseme_map[s])

            i += 1

        # print("\t%s - %s - %s", word, phonetic, ''.join(visemes_list))
        return ''.join(visemes_list)

    def _create_phonemes_group_list(self):
        lengths = set()
        for phonemes in self.phoneme_viseme_map:
            if len(phonemes) > 1:
                lengths.add(len(phonemes))
        self.groups_lengths = sorted(lengths, reverse=True)

import logging
import epitran
from src.encoding.encoders.transcription_exception import TranscriptionException


class IpaPolishEncoder:
    """
    Text to polish pronunciation encoder
    """

    def __init__(self):
        self.epi = epitran.Epitran('pol-Latn')

    def to_phonetics(self, word):
        """Converts polish word to IPA phonetics transcription. All text should be in lowercase"""
        transcription = self.epi.transliterate(word)
        if not transcription:
            msg = '\'%s\' could not be translated to phonemes' % transcription
            logging.error(msg)
            raise TranscriptionException(msg)
        return transcription

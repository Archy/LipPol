from src.encoding.encoders.ipa_english_encoder import IpaEnglishEncoder
from src.encoding.encoders.universal_encoder import UniversalEncoder


class EnglishEncoder:
    """
    Text to visems encoder
    Visems model taken from: https://docs.aws.amazon.com/polly/latest/dg/ph-table-english-uk.html
    """

    # Phonems to visems mapping model
    phoneme_viseme_map = {
        # Consonants
        'b': 'p',
        'd': 't',
        'dʒ': 'S',  # removed ͡
        'ð': 'T',
        'f': 'f',
        'g': 'k',
        'ɡ': 'k',  # only different encoding for g
        'h': 'k',
        'j': 'i',
        'k': 'k',
        'l': 't',
        'l̩': 't',
        'm': 'p',
        'm̩': 'p',
        'n': 't',
        'n̩': 't',
        'ŋ': 'k',
        'p': 'p',
        'ɹ': 'r',
        's': 's',
        'ʃ': 'S',
        't': 't',
        'tʃ': 'S',  # removed ͡
        'Θ': 'T',
        'θ': 'T',  # only different encoding for theta
        'v': 'f',
        'w': 'u',
        'z': 's',
        'ʒ': 'S',

        # Vowels
        'ə': '@',
        'əʊ': '@',
        'æ': 'a',
        'aɪ': 'a',
        'aʊ': 'a',
        'ɑː': 'a',
        'eɪ': 'e',
        'ɜː': 'E',
        'ɛ': 'E',
        'ɛə': 'E',
        'i:': 'i',
        'ɪ': 'i',
        'ɪə': 'i',
        'ɔː': 'O',
        'ɔɪ': 'O',
        'ɒ': 'O',
        'u:': 'u',
        'uː': 'u',
        'ʊ': 'u',
        'ʊə': 'u',
        'ʌ': 'E',

        # mapping missing from original model
        # TODO validate it
        'ɝ': 'E',  # like in bird
        'ɨ': '',  # like in rabbit
        'oʊ': '@',  # like in boat
        'ɔ': 'O',
        'ɑ': 'a',
        'i': 'i',
        'u': 'u',
        'a': '@',

        # missing from mapping but seems to not be present in cmudict
        # ɾ
        # ɾ̃
        # ʔ
        # ɚ
        # ʉ
    }
    ignored = ['ː', '-', '~', ' ']

    def __init__(self):
        self.encoder = UniversalEncoder(IpaEnglishEncoder(), EnglishEncoder.phoneme_viseme_map, EnglishEncoder.ignored)

    def to_visems(self, word):
        """Converts english word to visems"""
        return self.encoder.to_visems(word)

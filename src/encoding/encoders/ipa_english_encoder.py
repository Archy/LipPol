import logging
import cmudict
from src.encoding.encoders.transcription_exception import TranscriptionException


class IpaEnglishEncoder:
    """
    Text to english pronunciation encoder.
    """

    # CMU Pronouncing Dictionary returns ARPAbet pronunciation, so it needs to be converted to IPA.
    # Also epitran requires installing it manually so this way it's easier...
    # From https://en.wikipedia.org/wiki/ARPABET
    arpa2ipa = {
        # Consonants
        'B': 'b',
        'CH': 'tʃ',
        'D': 'd',
        'DH': 'ð',
        'DX': 'ɾ',  # missing from visemes map
        'EL': 'l̩',
        'EM': 'm̩',
        'EN': 'n̩',
        'F': 'f',
        'G': 'ɡ',
        'HH': 'h',
        'H': 'h',
        'JH': 'dʒ',
        'K': 'k',
        'L': 'l',
        'M': 'm',
        'N': 'n',
        'NG': 'ŋ',
        'NX': 'ɾ̃',  # missing from visemes map
        'P': 'p',
        'Q': 'ʔ',  # missing from visemes map
        'R': 'ɹ',
        'S': 's',
        'SH': 'ʃ',
        'T': 't',
        'TH': 'θ',
        'V': 'v',
        'W': 'w',
        'WH': 'ʍ',  # missing from visemes map
        'Y': 'j',
        'Z': 'z',
        'ZH': 'ʒ',

        # Vowels
        'AA': 'ɑ',
        'AE': 'æ',
        'AH': 'ʌ',
        'AO': 'ɔ',
        'AW': 'aʊ',
        'AX': 'ə',
        'AXR': 'ɚ',   # missing from visemes map
        'AY': 'aɪ',
        'EH': 'ɛ',
        'ER': 'ɝ',
        'EY': 'eɪ',
        'IH': 'ɪ',
        'IX': 'ɨ',
        'IY': 'i',
        'OW': 'oʊ',
        'OY': 'ɔɪ',
        'UH': 'ʊ',
        'UW': 'u',
        'UX': 'ʉ',  # missing from visemes map
    }

    def __init__(self):
        self.cmu_dict = cmudict.dict()

    def to_phonetics(self, text):
        """Converts english text to IPA phonetics transcription. All text should be in lowercase"""
        arpabet = self.cmu_dict[text]
        if not arpabet:
            msg = '\'%s\' could not be translated to phonemes' % text
            logging.error(msg)
            raise TranscriptionException(msg)
        arpabet = arpabet[0]

        ipa = []
        for symbol in arpabet:
            s = ''.join([i for i in symbol if not i.isdigit()])  # may contain 0/1/2 to indicate stress
            ipa.append(IpaEnglishEncoder.arpa2ipa.get(s, s))
        return ''.join(ipa)

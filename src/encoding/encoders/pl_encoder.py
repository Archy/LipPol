from src.encoding.encoders.ipa_polish_encoder import IpaPolishEncoder
from src.encoding.encoders.universal_encoder import UniversalEncoder

class PolishEncoder:
    """
    Text to visems encoder
    Visems model taken from: https://docs.aws.amazon.com/polly/latest/dg/ph-table-polish.html
    """

    # Phonems to visems mapping model
    phoneme_viseme_map = {
        # Consonants
        'b': 'p',
        'd': 't',
        'd͡z': 's',
        'd͡ʑ': 'J',
        'd͡ʐ': 'S',
        'f': 'f',
        'g': 'k',
        'h': 'k',
        'j': 'i',
        'k': 'k',
        'l': 't',
        'm': 'p',
        'n': 't',
        'ɲ': 'J',
        'p': 'p',
        'r': 'r',
        's': 's',
        'ɕ': 'J',
        'ʂ': 'S',
        't': 't',
        't͡s': 's',
        't͡ɕ': 'J',
        't͡ʂ': 'S',
        'v': 'f',
        'w': 'u',
        'z': 's',
        'ʑ': 'J',
        'ʐ': 'S',

        # Vowels
        'a': 'a',
        'ɛ': 'E',
        'ɛ̃': 'E',
        'i': 'i',
        'ɔ': 'O',
        'ɔ̃': 'O',
        'u': 'u',
        'ɨ': 'i',

        # mapping missing from original model
        # TODO validate it
        'ʲ': '',  # it's a softening and we can skip it ???
        'ɡ': 'k',
        'x': 'k',  # some sort of h ?? (h is represented by viseme k)
        'ŋ': 't',  # is's just n ?? (represented by viseme t)
        'ɾ': 'r',
        'ʔ': '',   # no fucking idea
        'ã': 'a',
        'o': 'O',
        'ũ': 'u',
        'e': 'E',
        'ĩ': 'i',
        'ẽ': 'E',
        'c': 'k',  # it's k ...
        'ä': 'a',
        'ɑ': 'a',
        'ʈ͡ʂ': 'S',  # it's 'cz'
        'ɟ': 'k',  # it's 'g'
        'ɳ': 't',
        'ʃ': 'S',  # it's 'sz'
        'ʒ': 'S',  # it's 'rz'
        'ɣ': 'k',  # it's 'h'
        't͡ʃ': 'S',  # it's 'cz'
    }
    ignored = ['ː', '-', ' ', '̃', ',', '!', '.']

    def __init__(self):
        self.encoder = UniversalEncoder(IpaPolishEncoder(), PolishEncoder.phoneme_viseme_map, PolishEncoder.ignored)

    def to_visems(self, word):
        return self.encoder.to_visems(word)

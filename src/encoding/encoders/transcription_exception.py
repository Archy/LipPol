class TranscriptionException(Exception):
    """Indicates failure in converting a word to phonetics"""
    pass

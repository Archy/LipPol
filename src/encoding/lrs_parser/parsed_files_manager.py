import os
from queue import Queue, Empty
from threading import Thread


class ParsedFilesManager(Thread):
    """Manages saving parsed files for disaster recovery"""

    def __init__(self, filepath):
        super().__init__()
        self.q = Queue()
        self.filepath = filepath
        self.is_running = True
        self.counter = 0

    def get_parsed_files(self):
        parsed = set()
        if os.path.exists(self.filepath):
            with open(self.filepath, 'r') as f:
                for line in f:
                    parsed.add(line.rstrip())
        return parsed

    def queue_parsed_file(self, full_path):
        self.q.put(full_path)

    def stop_running(self):
        self.is_running = False

    def run(self):
        with open(self.filepath, 'a') as out:
            while self.is_running or not self.q.empty():
                f = self._get_item()
                if f is None:
                    continue
                out.write(f + '\n')
                out.flush()
                self.counter += 1

    def _get_item(self):
        try:
            return self.q.get(True, 10.0)
        except Empty:
            return None
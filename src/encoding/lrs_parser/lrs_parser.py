import logging
import time
import os
from src.encoding.encoders.transcription_exception import TranscriptionException


def parse_file(dir_path, filename, results_path, file_manager, encoder, is_pretrain=False):
    """
    Parses LRS (BBC set) text files and creates new ones with visems transcription
    NOTE that this function is os-agnostic
    :param dir_path: full path to the directory that contains the file to parse
        eg: 'D:\\Datasets\\test\\translations\\pretrain\\5535415699068794046'
    :param filename: name of file to parse  (with .txt extension)
        eg. '00006.txt'
    :param results_path: base path to the directory with result files
        (directory that contains new 'main' and 'pretrain' directories)
        eg. 'D:\\Datasets\\test\\results'
    :param file_manager: Manager for submitting parsed filename for disaster recovery
    :param encoder: text to visemes encoder
    :param is_pretrain: if file is from the pretrained subset ('pretrain' subdirectory)
    """
    start_time = time.perf_counter()

    src_path = os.path.join(dir_path, filename)
    dst_path = get_dst_path(dir_path, filename, results_path)

    try:
        with open(src_path, encoding="utf-8") as src, open(dst_path, 'w', encoding="utf-8") as dst:
            trans = parse_common(src.readline(), dst, encoder)
            conf = src.readline()

            if is_pretrain:
                # skip empty line and headline
                src.readline()
                src.readline()

                dst.write('\n')
                for line in src:
                    parse_pretrain(line, dst, trans)

            dst.write('\n')
            dst.write(conf)
        # save file as parsed
        file_manager.queue_parsed_file(src_path)

        end_time = time.perf_counter()
        logging.debug('Parsing %s took %f seconds' % (src_path, end_time - start_time))
    except TranscriptionException as e:
        logging.error('Failed to parse %s, skipping. %s' % (src_path, e))


def parse_common(text, dst, encoder):
    """
    Parse first line of the transcription file (the full sentence)
    :param text: sentence to parse
    :param dst: file to write to
    :param encoder: text to visemes encoder
    :return: map containing word -> visemes transcriptions, for all words in passed text
    """
    text = text.replace('Text:', '').lstrip().rstrip()
    trans = encoder.sentence_to_visems(text, 'english')
    for i in range(2):
        write_sentence(dst, trans, i)
    return {word: visems for word, visems in trans}


def write_sentence(dst, trans, index):
    for t in trans:
        dst.write(t[index])
        dst.write(' ')
    dst.write('\n')


def parse_pretrain(line, dst, trans):
    """
    Parse a line of the additional data available in the pretrained subset
    :param line: line to parse
    :param dst: file to write to
    :param trans: map containing word->visems transcription, that has been acquired parsing the first line of the file
    :return:
    """
    word, *rest = line.split()
    for item in [word, trans[word], *rest[0:3]]:
        dst.write(item)
        dst.write(' ')
    dst.write('\n')

def write_space(dst, text):
    dst.write(text)
    dst.write(' ')


def get_dst_path(src_path, filename, results_path):
    """
    Creates destination path based on the source path, results base path and current file name.
    Creates all directories on the created destination path, if they don't exists.
    NOTE that this function is os-agnostic
    :param src_path: full path to the directory that contains the file to parse;
        eg: 'D:\\Datasets\\test\\translations\\pretrain\\5535415699068794046'
    :param filename: name of file to parse
        eg. '00006.txt'
    :param results_path: full base path to the directory with result files
        (directory that contains new 'main' and 'pretrain' directories)
        eg. 'D:\\Datasets\\test\\results'
    :return: created destination path
    """
    _, tmp = os.path.splitdrive(src_path)
    tmp, dir_path = os.path.split(tmp)
    _, type_path = os.path.split(tmp)

    dst_path = os.path.join(results_path, type_path, dir_path)
    os.makedirs(dst_path, exist_ok=True)
    return os.path.join(dst_path, filename)

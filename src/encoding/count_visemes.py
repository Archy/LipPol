from src.encoding.encoders.eng_encoder import EnglishEncoder
from src.encoding.encoders.pl_encoder import PolishEncoder


def load_visemes(phoneme_viseme_map):
    return set(filter(lambda v: len(v) > 0, phoneme_viseme_map.values()))


if __name__ == "__main__":
    eng_visemes = load_visemes(EnglishEncoder.phoneme_viseme_map)
    print(eng_visemes)
    print('English visemes: ', len(eng_visemes))

    pol_visemes = load_visemes(PolishEncoder.phoneme_viseme_map)
    print(pol_visemes)
    print('Polish visemes: ', len(pol_visemes))

    # combined
    total = eng_visemes | pol_visemes
    print(total)
    print('Total visemes: ', len(total))

import logging
# https://stackoverflow.com/questions/20240464/python-logging-file-is-not-working-when-using-logging-basicconfig
logging.basicConfig(filename='lrs.log', level=logging.DEBUG,
                    format='%(thread)-6d %(asctime)s %(levelname)-8s %(message)s')

import multiprocessing
import time
import os
from concurrent.futures import ThreadPoolExecutor
from src.encoding.encoders import visems_encoder
from src.encoding.lrs_parser import parsed_files_manager
from src.encoding.lrs_parser.lrs_parser import parse_file

DATA_PATH = 'D:\\Datasets\\extracted\\translations'
TEST_PATH = 'D:\\Datasets\\test\\translations'
OUT_TEST_PATH = 'D:\\Datasets\\test\\results'
MAIN_DIR = 'main'
PRETRAIN_DIR = 'pretrain'
MAX_QUEUED_FILES = 1000


def main():
    start_time = time.perf_counter()
    main_path = os.path.join(TEST_PATH, MAIN_DIR)
    pretrain_path = os.path.join(TEST_PATH, PRETRAIN_DIR)

    file_manager = parsed_files_manager.ParsedFilesManager('parsed.txt')
    file_manager.start()

    parsed_files = file_manager.get_parsed_files()
    with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
        main_parsed = parse_dir(main_path, executor, file_manager, parsed_files, False)
        pretrain_parsed = parse_dir(pretrain_path, executor, file_manager, parsed_files, True)

    file_manager.stop_running()
    file_manager.join()

    end_time = time.perf_counter()
    logging.info('Main files scheduled for parsing: %d', main_parsed)
    logging.info('Pretrain files scheduled for parsing: %d', pretrain_parsed)
    logging.info('Total files scheduled for parsing: %d', (main_parsed + pretrain_parsed))
    logging.info('Files parsed: %d', file_manager.counter)
    logging.info('Total time taken: %f seconds', (end_time - start_time))


def parse_dir(path, executor, file_manager, parsed_files, is_pretrained):
    counter = 0
    encoder = visems_encoder.VisemsEncoder()

    for folderName, _, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith('.txt'):
                full_path = os.path.join(folderName, filename)
                if full_path not in parsed_files:
                    # limit the number of queued files (there are ~150.000 files to parse...)
                    while executor._work_queue.unfinished_tasks > MAX_QUEUED_FILES:
                        time.sleep(1)
                    executor.submit(parse_file, folderName, filename, OUT_TEST_PATH, file_manager, encoder, is_pretrained)
                    counter += 1
                    if counter % 100 == 0:
                        logging.info('Scheduled files: %d', counter)
    return counter


def test():
    start_time = time.perf_counter()
    file_manager = parsed_files_manager.ParsedFilesManager('parsed.txt')
    file_manager.start()

    encoder = visems_encoder.VisemsEncoder()
    working_dir = r'C:\Users\Jan\Desktop\LipNet\LipPol\src\encoding\lrs_parser'

    parse_file(working_dir, 'main.txt', working_dir, file_manager, encoder)
    parse_file(working_dir, 'pretrain.txt', working_dir, file_manager, encoder, False)
    parse_file(working_dir, 'pretrain.txt', working_dir, file_manager, encoder, True)

    file_manager.stop_running()
    file_manager.join()
    end_time = time.perf_counter()
    logging.info('Total time taken: %f seconds', (end_time - start_time))


if __name__ == "__main__":
    logging.disable(logging.NOTSET)  # sth overrides this, no idea what
    # test()
    main()

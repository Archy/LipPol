from keras.layers.convolutional import Conv3D, ZeroPadding3D
from keras.layers.pooling import MaxPooling3D
from keras.layers.core import Dense, Activation, SpatialDropout3D, Flatten, Lambda
from keras.layers.wrappers import Bidirectional, TimeDistributed
from keras.layers.recurrent import GRU
from keras.layers.normalization import BatchNormalization
from keras.layers import Input, ReLU
from keras.models import Model
from keras.optimizers import Adam
from keras import backend as K
import tensorflow as tf


def _ctc(args):
    """
    Calculate loss according to CTC algorithm
    :param args: tuple of:
        softmax outputs; shape (batch size, frames_count, output_size),
        target labels; shape (batch_size, labels length)
        target labels length; shape (batch_size, 1)
        input frames length; shape (batch_size, 1)
    :return:
    """
    y_pred, labels, label_length, input_length = args
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)


def ctc_decode(args):
    """
    Decodes the output of a softmax using greedy search (best path)
    :param args: tuple of:
        softmax_outputs; shape (batch size, frames_count, output_size),
        inputs_lengths; shape (batch_size, 1)
    """
    y_pred, input_lengths = args
    seq_len = tf.squeeze(input_lengths, axis=1)

    top_k_decoded, _ = K.ctc_decode(y_pred, seq_len)
    return top_k_decoded[0]


def _relu6(name):
    return ReLU(max_value=6.0, name=name)


class LipNet:
    def __init__(self, frame_width, frame_height, output_size) -> None:
        """
        Creates the LipNet model.
        :param output_size: nr of output
        """
        super().__init__()

        self.input_shape = (None, frame_height, frame_width, 3)

        # Note: below model is heavily based on rizkiarm lipnet
        self.the_input = Input(shape=self.input_shape, name='the_input', dtype='float32')

        # create STCNN layers
        x = ZeroPadding3D(padding=(1, 2, 2), name='zero1')(self.the_input)
        x = Conv3D(32, (3, 5, 5), strides=(1, 2, 2), kernel_initializer='he_normal', name='conv1')(x)
        x = BatchNormalization(name='batc1')(x)
        x = Activation('relu', name='actv1')(x)
        # x = _relu6('actv1')(x)
        x = SpatialDropout3D(0.5, )(x)
        x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), name='max1')(x)

        x = ZeroPadding3D(padding=(1, 2, 2), name='zero2')(x)
        x = Conv3D(64, (3, 5, 5), strides=(1, 1, 1), kernel_initializer='he_normal', name='conv2')(x)
        x = BatchNormalization(name='batc2')(x)
        x = Activation('relu', name='actv2')(x)
        # x = _relu6('actv2')(x)
        x = SpatialDropout3D(0.5)(x)
        x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), name='max2')(x)

        x = ZeroPadding3D(padding=(1, 1, 1), name='zero3')(x)
        x = Conv3D(96, (3, 3, 3), strides=(1, 1, 1), kernel_initializer='he_normal', name='conv3')(x)
        x = BatchNormalization(name='batc3')(x)
        x = Activation('relu', name='actv3')(x)
        # x = _relu6('actv3')(x)
        x = SpatialDropout3D(0.5)(x)
        x = MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), name='max3')(x)

        x = TimeDistributed(Flatten())(x)

        # RNN layers
        x = Bidirectional(GRU(256, return_sequences=True, kernel_initializer='Orthogonal', name='gru1'), merge_mode='concat')(x)
        x = Bidirectional(GRU(256, return_sequences=True, kernel_initializer='Orthogonal', name='gru2'), merge_mode='concat')(x)

        # predictions - layers below are specific to lippol and won't be loaded from pretrained rizkiarm LipNet
        self.dense = Dense(output_size, kernel_initializer='he_normal', name='dense1_pl')(x)
        self.y_pred = Activation('softmax', name='softmax_pl')(self.dense)

        # CTC loss
        self.labels = Input(shape=[None], name='labels', dtype='float32')
        self.labels_length = Input(shape=[1], name='label_length', dtype='int64')
        self.input_length = Input(shape=[1], name='input_length', dtype='int64')

        args = [self.y_pred, self.labels, self.labels_length, self.input_length]
        self.ctc = Lambda(_ctc, output_shape=(1,), name='ctc')(args)

        # model
        inputs = [self.the_input, self.input_length, self.labels, self.labels_length]
        self.model = Model(inputs=inputs, outputs=self.ctc)

        # decoding
        self.ctc_decoded = Lambda(ctc_decode, name='decoder')([self.y_pred, self.input_length])
        self.decoder = K.function([self.the_input, self.input_length], [self.ctc_decoded])

    def compile(self, lr=0.0001):
        """
        Compiles the model. Uses adam as optimizer
        :param lr: learning rate
        """
        optimizer = Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
        self.model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=optimizer)

    def predict(self, x):
        """
        Predicts output for given single sample of frames
        :param x: single sample of frames. Must have shape (frames_count, frame_height, frame_width, 3)
        :return: predicted output
        """
        decoded = self.decoder([[x], [[len(x)]]])
        return decoded[0][0]

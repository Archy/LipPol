#!/usr/bin/env python3
import numpy as np

from src.config.config import config
from src.dataset.bbc_set import load_bbc, SetType
from src.dataset.encoder import BbcEncoder
from src.encoding.encoders.transcription_exception import TranscriptionException
from src.lipnet.model import LipNet
from src.video.mouth_extractor import MouthExtractor
from src.video.mouth_not_found_exception import MouthNotFoundException

# path to file with weight to be used
weights_path = r'D:\Datasets\lippol_checkpoints\weights55.h5'
mouth_extractor = MouthExtractor()
bbc = load_bbc()
encoder = BbcEncoder()


def print_expected_visemes(filepath):
    """
    Prints expected text and visemes (converts the text automatically)
    """
    with open(filepath, encoding="utf-8") as src:
        text = src.readline().replace('Text:', '').lstrip().rstrip()
        visemes = ' '.join(list(map(lambda x: x[1], encoder.encoder.sentence_to_visems(text, 'english'))))
        print('Expected text: ' + text)
        print('Expected visemes: ' + visemes)


def get_sample():
    """
    Returns single random sample from TRAIN set.
    :return: sample's frames
    """
    frames = None
    while frames is None:
        try:
            text_path, video_path = bbc.get_sample(SetType.TRAIN, mouth_extracted=False)
            frames = mouth_extractor.extract_mouth_bbc(video_path + '.mp4')
            print('Sample: ' + str(text_path))
            print_expected_visemes(text_path + '.txt')
        except (MouthNotFoundException, TranscriptionException) as e:
            frames = None

    return np.asarray(frames)


def out_to_visemes(out):
    visemses = encoder.output_to_visemes(out)
    print('Actual output: ' + visemses)


def predict():
    """
    Initiates LipNet model, loads pretrained weights and runs prediction for given number of random samples.
    """
    bbc.reset_epoch_subset(SetType.TEST)
    lippol = LipNet(config['frame_width'],
                    config['frame_height'],
                    config['output_size'])
    lippol.compile(config['learing_rate'])
    lippol.model.load_weights(weights_path)

    for i in range(3):
        x = get_sample()
        out = lippol.predict(x)
        out_to_visemes(out)


if __name__ == "__main__":
    predict()

#!/usr/bin/env python3

import logging
import datetime
import json
from src.config.config import config
logging.basicConfig(filename=config['train_log_path'], level=logging.DEBUG,
                    format='%(thread)-6d %(asctime)s %(levelname)-8s %(message)s')
from src.dataset.bbc_set import SetType
from src.dataset.generator import BatchGenerator
from src.lipnet.model import LipNet
from keras.callbacks import ModelCheckpoint, CSVLogger


def train():
    generator = BatchGenerator()

    lippol = LipNet(config['frame_width'],
                    config['frame_height'],
                    config['output_size'])
    lippol.compile(config['learing_rate'])

    if config['use_rizkiarm_weights']:
        logging.info('Using pretrained rizkiarm LipNet weights')
        lippol.model.load_weights(config['rizkiarm_weights_path'], by_name=True)

    epochs = config['epochs']
    steps_per_epoch = config['steps_per_epoch']
    validation_steps = config['validation_steps']
    callbacks = [
        ModelCheckpoint(config['checkpoints_path'], save_weights_only=True, mode='auto', period=1),
        CSVLogger(config['csvlog_path'], separator=',', append=False)
    ]

    history = lippol.model.fit_generator(
        generator=generator.generator(SetType.TRAIN, steps_per_epoch),
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        verbose=2,
        callbacks=callbacks,
        validation_data=generator.generator(SetType.VAL, validation_steps),
        validation_steps=validation_steps,
        max_queue_size=20,
        workers=1,  # has to be 1, current generator is not thread safe (well it is, but no according to python ...)
        initial_epoch=0  # TODO allow to resume learning from arbitrary epoch (loading trained weights)
    )
    with open('history.json', 'w') as f:
        json.dump(history.history, f)


if __name__ == "__main__":
    logging.disable(logging.NOTSET)
    logging.info('Starting at: %s', str(datetime.datetime.now()))
    train()
    logging.info('Finished at: %s', str(datetime.datetime.now()))

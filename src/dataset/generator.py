import os
import logging
import time

import numpy as np

from src.dataset.bbc_set import load_bbc, SetType
from src.dataset.encoder import BbcEncoder
from src.dataset.mouths_loader import MouthLoader
from src.dataset.too_long_movie_exception import TooLongMovieException
from src.encoding.encoders.transcription_exception import TranscriptionException
from src.config.config import config

# TODO fix: 'ValueError: could not broadcast input array from shape (50,100,3) into shape (50)'
# ^^ seems to be fixed, but better be cautious, cause it's hard to reproduce


class BatchGenerator:
    def __init__(self) -> None:
        super().__init__()
        self.bbc = load_bbc()
        self.mouth_loader = MouthLoader()
        self.encoder = BbcEncoder()

        self.batch_size = config['batch_size']

    def generator(self, set_type, samples_per_epoch):
        """
        Generator for keras fit_generator. Generates batches of data
        """
        while True:
            for i in range(samples_per_epoch):
                yield self.get_batch(set_type)
            self.bbc.reset_epoch_subset(set_type)

    def get_batch(self, set_type=SetType.TRAIN):
        """
        Return a batch of data from given bbc subset.
        Targets labels (output) are padded to the length of longest target

        """
        start = time.time()
        frames = []
        frames_lengths = []
        targets = []
        targets_lengths = []

        while len(frames) < self.batch_size:
            sample = self._get_sample(set_type)
            if sample is not None:
                f, f_len, t, t_len = sample
                frames.append(f)
                frames_lengths.append(f_len)
                targets.append(t)
                targets_lengths.append(t_len)

        frames = self._pad_frames_batch(frames)
        frames_lengths = np.asarray(frames_lengths)
        targets = self._pad_targets_batch(targets)
        targets_lengths = np.asarray(targets_lengths)

        logging.info('Creating a batch took: %f', time.time() - start)
        return [frames, frames_lengths, targets, targets_lengths], targets

    def _get_sample(self, set_type):
        """
        Return sample of data as: (frames, frames length, target visemes, lenght of target visemes sequence)
        of None if sample was invalid.
        """
        text_path, mouths_path = self.bbc.get_sample(set_type)

        if not os.path.exists(mouths_path + '.avi'):
            logging.debug('Video with extracted mouth doesn\'t exists. Skipping. (%s)', mouths_path)
            return None

        try:
            target = self.encoder.parse_file(text_path + '.txt')
            target_len = len(target)

            frames = self.mouth_loader.load_mouths(mouths_path + '.avi')
            frames_len = len(frames)

            if (target_len + 3) >= frames_len:
                # tensorflow fails on output longer than input
                logging.error('Wrong sample: %s. Target longer than frames' % text_path)
                self.bbc.remove_invalid_sample(text_path, SetType.TRAIN)
                return None

            return frames, frames_len, target, target_len
        except (TranscriptionException, TooLongMovieException) as e:
            logging.error('Wrong sample: %s, skipping. %s' % (text_path, e))
            self.bbc.remove_invalid_sample(text_path, SetType.TRAIN)
            return None
        except TooLongMovieException as e:
            logging.error('Too long movie (%s), skipping. %s' % (mouths_path, e))
            self.bbc.remove_invalid_sample(text_path, SetType.TRAIN)
            return None
        except Exception as e:
            logging.error('Sample \'%s\': unknown exception: %s' % (mouths_path, e))
            return None

    def _pad_targets_batch(self, targes_batch):
        """
        Pads all target labels sequences in batch to the length of longest labels sequence in passed batch.
        """
        max_len = max(map(lambda x: len(x), targes_batch))
        logging.debug('Padding batch\'s frames to %d', max_len)

        targets = []
        for t in targes_batch:
            targets.append(self._pad_target(t, max_len))
        return np.asarray(targets)

    def _pad_target(self, target, max_targets):
        """
        Pads targets visemes sequences to given length
        """
        missing_targets = max_targets - len(target)
        if missing_targets > 0:
            return np.pad(np.asarray(target), [(0, missing_targets)], mode='constant', constant_values=0)
        else:
            return np.asarray(target)

    def _pad_frames_batch(self, frames_batch):
        """
        Pads all frames sequences in batch to the length of longest frames sequence in passed batch.
        (memory expensive)
        """
        max_len = max(map(lambda x: len(x), frames_batch))
        logging.debug('Padding batch\'s frames to %d', max_len)

        frames = []
        for f in frames_batch:
            frames.append(self._pad_frames(f, max_len))
        return np.asarray(frames)

    def _pad_frames(self, frames, max_frames):
        """Pads single frames sequence to given length"""
        missing_frames = max_frames - len(frames)
        if missing_frames > 0:
            return np.pad(np.asarray(frames), [(0, missing_frames), (0, 0), (0, 0), (0, 0)], mode='constant', constant_values=0)
        else:
            return np.asarray(frames)

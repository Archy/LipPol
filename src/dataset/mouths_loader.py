import logging
import cv2
import time

from src.config.config import config
from src.dataset.too_long_movie_exception import TooLongMovieException


class MouthLoader:
    def __init__(self) -> None:
        super().__init__()
        self.max_allowed_frames = config['max_allowed_frames']
        self.print_when_loading = config['print_mouths_when_loading']

    def load_mouths(self, video_path):
        """Loads all frames from given video into array if images"""
        start = time.time()

        vidcap = cv2.VideoCapture(video_path)
        mouth_frames = []
        count = 0

        try:
            if self.print_when_loading:
                print(video_path + " {")
            success, image = vidcap.read()
            while success:
                mouth_frames.append(image)
                success, image = vidcap.read()
                count = count + 1
                if count > self.max_allowed_frames:
                    raise TooLongMovieException('Too long movie clip')

            logging.debug('Mouth loading (%d frames) took: %f', count, time.time() - start)
            return mouth_frames
        finally:
            if self.print_when_loading:
                print("} " + video_path)
            vidcap.release()

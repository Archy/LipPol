class TooLongMovieException(Exception):
    """Indicates a movie that won't fit into memory"""
    pass

from enum import Enum
from threading import Lock

from src.config.config import config
import os
import logging
from readerwriterlock import rwlock


class SetType(Enum):
    """Subset types as defined by BBC data set"""
    PRETRAIN = 1,
    TRAIN = 2,
    TEST = 3,
    VAL = 4


class BbcSet:
    """Represents whole BBC data set. It's probably thread safe"""
    def __init__(self, sets, base_path) -> None:
        super().__init__()
        self.sets = sets
        self.base_path = os.path.join(base_path, 'mvlrs_v1')

        self.epoch_sets = {}
        for t in [SetType.TRAIN, SetType.VAL]:
            self.epoch_sets[t] = self.sets[t].copy()
            logging.info('Set \'%s\' size: %d', t, len(self.epoch_sets[t]))

        self.epoch_lock = rwlock.RWLockRead()
        self.sets_lock = Lock()

    def get_set(self, setType: SetType):
        return self.sets[setType]

    def reset_epoch_subset(self, set_type):
        """
        Resets subset for new epoch.
        """
        with self.epoch_lock.gen_wlock(), self.sets_lock:
            self.epoch_sets[set_type] = self.sets[set_type].copy()
            logging.info('Set \'%s\' size: %d', set_type, len(self.epoch_sets[set_type]))

    def get_sample(self, s_type=SetType.TRAIN, mouth_extracted=True):
        """
        Returns a sample (absolute paths to text and mouths video) from a given set. Note that sample will be
        removed from the epoch subset.
        Currently works only for train and validation sets
        """
        with self.epoch_lock.gen_rlock():
            if s_type == SetType.PRETRAIN:
                set_dir = 'pretrain'
            else:
                set_dir = 'main'
            sample = self.epoch_sets[s_type].pop()

            # make it os agnostic
            sample = sample.split('/')
            assert len(sample) == 2

            text_path = os.path.join(self.base_path, set_dir, *sample)
            if mouth_extracted:
                mouths_path = os.path.join(self.base_path, 'mouth', *sample)
            else:
                mouths_path = text_path

            return text_path, mouths_path

    def remove_invalid_sample(self, sample, s_type):
        """
        Removes sample that can't be used (eg. to long video or text not possible to transcribe to visemes)
        """
        sample, tmp1 = os.path.split(sample)
        sample, tmp2 = os.path.split(sample)
        key = tmp2 + '/' + tmp1

        with self.sets_lock:
            if key in self.sets[s_type]:
                self.sets[s_type].remove(key)
        logging.info('Removing invalid sample from %s, remaining: %d', str(s_type), len(self.sets[s_type]))


SETS_DICT = {
    SetType.PRETRAIN: 'pretrain.txt',
    SetType.TRAIN: 'train.txt',
    SetType.TEST: 'test.txt',
    SetType.VAL: 'val.txt'
}


def load_bbc() -> BbcSet:
    sets = {}
    for key, file_name in SETS_DICT.items():
        subset = set()
        with open(os.path.join(config['bbc_base_path'], file_name), encoding='UTF-8') as file:
            for line in file:
                # remove \n at the end of line
                line = line.strip()
                subset.add(line)
        sets[key] = subset

    return BbcSet(sets, config['bbc_base_path'])

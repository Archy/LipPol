import logging
import time

from src.encoding.encoders.visems_encoder import VisemsEncoder
from src.encoding.encoders.eng_encoder import EnglishEncoder


class BbcEncoder:
    def __init__(self) -> None:
        super().__init__()
        self.encoder = VisemsEncoder()
        self.visemes_set = set(filter(lambda v: len(v) > 0, EnglishEncoder.phoneme_viseme_map.values()))

        self.vis2target = {val: idx for (idx, val) in enumerate(self.visemes_set)}
        self.out2vis = {val: key for (key, val) in self.vis2target.items()}

        self.space = len(self.out2vis)
        self.out2vis[self.space] = ' '
        self.ctc_blank = len(self.out2vis)
        self.out2vis[self.ctc_blank] = ''

    def visemes_to_target(self, visemes):
        """
        Converts visemes sequence to LipNet input array
        """
        target = []
        for v in visemes:
            target.append(self.vis2target[v])
        return target

    def output_to_visemes(self, output):
        """
        Converts LipNet output array to visemes sequence
        """
        visemes = []
        for out in output:
            visemes.append(self.out2vis[out])
        return ''.join(visemes)

    def parse_file(self, filepath):
        """
        Parses given LRS (BBC) file and return target labels array (ready to be fed to model)
        """
        start_time = time.perf_counter()

        with open(filepath, encoding="utf-8") as src:
            target = self._parse_common(src.readline())

        logging.debug('Parsing %s took %f seconds' % (filepath, time.perf_counter() - start_time))
        return target

    def _parse_common(self, text):
        """
        Converts given text to target labels array
        """
        target = []

        text = text.replace('Text:', '').lstrip().rstrip()
        trans = self.encoder.sentence_to_visems(text, 'english')
        for t in trans:
            target.extend(self.visemes_to_target(t[1]))
            target.append(self.space)
        # remove last space
        target = target[:-1]
        return target

# LipPol - lipreading tool for polish language
Implementation of LipNet based on visemes prediction.
Please create a Merge Request before merging to master


## Prerequisites
All python prerequisites are listed in requirements.txt.
In addition to that you'll also need:
* docker & nvidia-docker (if you want to run it in docker)

To run training you'll need LRS (BBC) dataset with already extracted mouth. You can get it by running convert_bbc.py on standard LRS datataset.
Seems it's faster on BBC (took me ~5h to process about 7000 movies (vs 7-8h to process 3000 grid movies))

### Expected dataset directory structure:
```
.
├── pretrain.txt    # files with listed bbc dataset samples. Already in bbc dataset (also in our reduced bbc set)
├── test.txt
├── train.txt
├── val.txt
├── mvlrs_v1        # direcory with actual data
    ├── pretrain    # directory with pretrain samples (already exists in dataset)
    ├── main        # directory with train, validation and test samples (already exists in dataset)
    ├── mouth       # direcory with movies with extracted mouths. Needs to be created!
        ├── 5535415699068794046
        |   ├── 00001.avi
        |   ├── 00002.avi
        |   ...
        ├── 5535415699068794046
        ...
```
*mouth* directory has exactly the same structure as main directory, except it contains movies with extracted mouth.


## How to train
* in *src/config/config.py*, adjust *batch_size* and *max_allowed_frames* according to you GPU memory.
  * You'll have to experiment with those values a bit to be sure that you won't get OOM error.
  * *steps_per_epoch* needs to be adjusted as well according to number valid samples you'll get
* the first epoch might have a lot of invalid samples (too long movies or text that can't be transcribed to visemes), but it gets better on further epochs

### Windows
* adjust paths in *src/config/config.py*
* install needed packages: ```pip3 install -r requirements.txt```
* note that installing dlib on Windows could be problematic. I had to install cmake (*pip3 install cmake*) to get it work.
* start training (*train.py file*)

### Docker
* adjust paths in *main.sh*
* start training inside a container: `sudo ./main.sh bash -c "ldconfig && ./src/lipnet/train.py"` (the `ldconfig` is to prevent `cannot open shared object file` errors)

## Running prediction
* adjust paths in *src/config/config.py* if needed
* in *src/lipnet/predict.py* adjust *weights_path* and run it

## Known issues

### Errors like `[mjpeg @ 0x7f69b88d9c00] error y=3 x=6`
These seem to be actual corruptions in the mouth video files because I also get them when I play the files in VLC or `mpv`. (Example file with errors: `mvlrs_v1/mouth/5540483330981347168/00016.avi`.) However, the files are still readable so this may not be worth investigating further.

If you want to find the files which cause errors, set the config value `print_mouths_when_loading` to `True`. This will cause the program to print the names of input files as they're loaded.
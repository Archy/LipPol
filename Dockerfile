FROM tensorflow/tensorflow:1.9.0-gpu-py3

RUN apt-get update && apt-get -y install git python3 python3-pip cmake libboost-python-dev libgtk2.0-dev
RUN pip3 install --upgrade pip
# libgtk2.0-dev is needed for opencv

WORKDIR /root
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# These variables are at the end so that the commands above aren't rerun each time they're changed.
ENV DATASET=/root/dataset
ENV WORKDIR=/root/workdir
ENV TRAINING_DIR=/root/workdir/training
ENV PYTHONPATH=${WORKDIR}

WORKDIR ${WORKDIR}

import os
import unittest
import src.dataset.bbc_set as bbc

SETS_DICT = {
    bbc.SetType.PRETRAIN: 4276,
    bbc.SetType.TRAIN: 6916,
    bbc.SetType.TEST: 1243,
    bbc.SetType.VAL: 1082
}


class VideoLoadingTest(unittest.TestCase):
    def test_bbc_load(self):
        bbc_set = bbc.load_bbc()
        for key, count in SETS_DICT.items():
            self.assertEqual(len(bbc_set.get_set(key)), count)

        bbc_set.start_epoch()
        sample, mouths_path = bbc_set.get_sample()

        self.assertTrue(os.path.exists(sample + '.txt'))
        self.assertTrue(os.path.exists(sample + '.mp4'))

from src.video.mouth_extractor import MouthExtractor
import unittest
import cv2
import os
import logging


test_video_path = r"D:\Datasets\BBC_reduced\small\small\mvlrs_v1\main\5535415699068794046\00001.mp4"

logging.basicConfig(filename='test.log', level=logging.DEBUG,
                    format='%(thread)-6d %(asctime)s %(levelname)-8s %(message)s')


class VideoLoadingTest(unittest.TestCase):
    def test_bbc_load(self):
        extractor = MouthExtractor()
        frames = extractor.extract_mouth_bbc(test_video_path)

        base = os.path.join('imgs', 'mouth')
        ext = '.png'
        for i, f in enumerate(frames):
            cv2.imwrite(base + str(i) + ext, f)
            self.assertEqual(f.shape, (50, 100, 3))

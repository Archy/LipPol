import numpy as np
import unittest
import logging
from src.video.mouth_extractor import MouthExtractor
from src.lipnet.model import LipNet
from keras.utils import to_categorical

test_video_path = r"D:\Datasets\BBC_reduced\small\small\mvlrs_v1\main\5535415699068794046\00001.mp4"

logging.basicConfig(filename='test.log', level=logging.DEBUG,
                    format='%(thread)-6d %(asctime)s %(levelname)-8s %(message)s')

V_SIZE = 10


def mock_output():
    # batch_size x frames_count x correct_output
    o = []
    for i in range(35):
        o.append(to_categorical(1, V_SIZE))
    return np.asarray([o])


def mock_input(frames):
    input = []
    # expected input:
    #   1 x 35 x 50 x 100       frames
    #   1                       frames count (=35)
    #   1 x ?                   expected output
    #   1                       expected output length
    input.append(np.asarray([frames]))
    input.append(np.asarray([len(frames)], dtype=np.int64))
    input.append(np.zeros((1, 25)))
    input.append(np.asarray([10], dtype=np.int64))

    return input


class VideoLoadingTest(unittest.TestCase):
    def test_bbc_load(self):
        extractor = MouthExtractor()
        frames = extractor.extract_mouth_bbc(test_video_path)
        input = mock_input(frames)

        output = mock_output()

        lippol = LipNet(100, 50, len(frames), 25, V_SIZE)
        print(lippol.model.summary())
        lippol.compile()
        lippol.model.test_on_batch(input, np.array(output))

import unittest
from src.dataset.encoder import BbcEncoder

TEST_FILE_PATH = r'D:\Datasets\BBC_reduced\small\small\mvlrs_v1\main\5535415699068794046\00001.txt'
EXPECTED = 'uEt iur kukik Sips at k@p'


class BbcEncoderTest(unittest.TestCase):
    def test_encode(self):
        encoder = BbcEncoder()

        target = encoder.parse_file(TEST_FILE_PATH)
        decoded = encoder.output_to_visemes(target)
        self.assertEqual(decoded, EXPECTED)

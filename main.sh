#!/usr/bin/env bash

dataset_path="/media/jan/F44EF69A4EF65538/Datasets/BBC_reduced/small/small"
lippol_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
image_name="lippol:latest"

error() {
    printf "ERROR: %s\\n" "$1" 1>&2
}

fatal_error() {
    error "$1"
    exit 1
}

if [ "$EUID" -ne 0 ]; then
    fatal_error "This script must be run with root privileges."
fi

mkdir -p -- "$lippol_path/training/weights"

# If we got arguments, use them as command to execute inside the container.
if [ $# -gt 0 ]; then
    command=("$@")
else
    command=("/bin/bash")
fi

docker build -t "$image_name" .
docker run --runtime=nvidia \
       -v "$lippol_path:/root/workdir" \
       -v "$dataset_path:/root/dataset" \
       -it "$image_name" "${command[@]}"
